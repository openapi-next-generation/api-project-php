#!/usr/bin/env bash
set -e
cd "$(dirname "${BASH_SOURCE[0]}")/.."
eval $(egrep -v '^#' .env | xargs)


docker-compose run --rm php "${@-ash}"