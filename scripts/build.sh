#!/usr/bin/env bash
set -e
cd "$(dirname "${BASH_SOURCE[0]}")/.."
eval $(egrep -v '^#' .env | xargs)


docker build -f ./docker/nginx/Dockerfile --tag ${NGINX_IMAGE}:latest .
docker build -f ./docker/php/Dockerfile --tag ${PHP_IMAGE}:latest .
if [ ! -f ./composer.phar ]
then
    wget -O ./composer.phar https://getcomposer.org/composer.phar
fi
docker run --rm -v ${PWD}:/var/www ${PHP_IMAGE} php composer.phar install -o

target=release
if [[ "$1" == "dev" ]]
then
    target=dev
fi
docker build -f ./docker/php/Dockerfile --tag ${PHP_IMAGE}:latest --target ${target} .