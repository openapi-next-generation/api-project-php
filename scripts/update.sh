#!/usr/bin/env bash
set -e
cd "$(dirname "${BASH_SOURCE[0]}")/.."

#
# use project name from composer.json and update .env project name env variables
#
project_name=$(jq -r .name composer.json | cut -d/ -f2)
sed -ri \
"s/^\s*(PROJECT_NAME\s*=)\s*(.*)\s*$/\1"${project_name}"/ ; "\
"s/^\s*(NGINX_IMAGE\s*=)\s*(.*)\s*$/\1"${project_name}_nginx"/ ; "\
"s/^\s*(PHP_IMAGE\s*=)\s*(.*)\s*$/\1"${project_name}_php"/" \
.env

#
# generate actions, api entities and api tests
#
./scripts/bash.sh bin/console generate-all
