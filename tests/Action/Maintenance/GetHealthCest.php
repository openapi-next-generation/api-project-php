<?php

class GetHealthCest
{

    public function basic(\ApiTester $I)
    {
        $I->sendGET('/health');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }


}
