<?php

class GetReadyCest
{

    public function basic(\ApiTester $I)
    {
        $I->sendGET('/ready');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }


}
