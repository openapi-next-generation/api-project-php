<?php

use Monolog\Logger;
use OpenapiNextGeneration\ApiProjectToolsPhp\Api\OpenapiGeneratorProvider;
use OpenapiNextGeneration\ApiProjectToolsPhp\Application\ApplicationProvider;
use OpenapiNextGeneration\ApiProjectToolsPhp\Config\ConfigProvider;
use OpenapiNextGeneration\ApiProjectToolsPhp\Error\ErrorHandler;
use OpenapiNextGeneration\ApiProjectToolsPhp\Error\LoggerProvider;
use OpenapiNextGeneration\ApiProjectToolsPhp\Request\RequestProvider;
use OpenapiNextGeneration\ApiProjectToolsPhp\Route\ActionGeneratorProvider;
use OpenapiNextGeneration\ApiProjectToolsPhp\Route\DispatcherProvider;

$container = new \Pimple\Container();

ErrorHandler::register();
$container->register(new LoggerProvider());
ErrorHandler::instance()->setLogger($container[Logger::class]);

$container->register(new OpenapiGeneratorProvider());
$container->register(new ApplicationProvider());
$container->register(new ConfigProvider());
$container->register(new RequestProvider());
$container->register(new DispatcherProvider());
$container->register(new ActionGeneratorProvider());

return $container;
