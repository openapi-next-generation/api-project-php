<?php
/* @var \Pimple\Container $container */
/* @var \Symfony\Component\Console\Application $app */

use OpenapiNextGeneration\ApiProjectToolsPhp\Console\GenerateAllCommand;

$app->add(new GenerateAllCommand($container));
