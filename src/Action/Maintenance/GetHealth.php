<?php

namespace App\Action\Maintenance;

use OpenapiNextGeneration\ApiProjectToolsPhp\Action\AbstractAction;
use Pimple\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GetHealth extends AbstractAction
{
    const HTTP_METHOD = 'GET';
    const ROUTE = '/health';


    public function __invoke(Request $request, Container $container): Response
    {
        return new JsonResponse([
            'status' => 'I am alive'
        ]);
    }
}