<?php

use OpenapiNextGeneration\ApiProjectToolsPhp\Application\Application;
use Symfony\Component\HttpFoundation\Request;

require '../vendor/perftools/xhgui-collector/external/header.php';

chdir(__DIR__ . '/..');
require 'vendor/autoload.php';

/* @var $container \Pimple\Container */
$container = require 'src/container.php';

/* @var $app Application */
$app = $container[Application::class];
$request = $container[Request::class];
$response = $app->handle($request);
$response->send();
$app->terminate($request, $response);